# SailfishOS on Xperia™ Z3 Tablet Compact (Wifi) (SGP611)

![SailfishOS](https://sailfishos.org/wp-content/themes/sailfishos/icons/apple-touch-icon-120x120.png)

## Current Build
2.2.1.18 - [![pipeline status](https://gitlab.com/sailfishos-porters-ci/sailfish-scorpion_windy_ci/badges/master/pipeline.svg)](https://gitlab.com/sailfishos-porters-ci/sailfish-scorpion_windy_ci/commits/master)

## Install
**!!!! Backup your data you do it on your own risk !!!!**

* unlock the bootloader check [Sony Open Devices](https://developer.sony.com/develop/open-devices/)
* flash [AOSP 6.0.1_r80](http://195.201.99.111/sailfishos-scorpion_windy/scorpion_windy-aosp-r80.tar.gz) on your device.

```
fastboot -S 256M flash boot boot.img
fastboot -S 256M flash system system.img
fastboot -S 256M flash userdata userdata.img
```
* flash [twrp](https://eu.dl.twrp.me/scorpion_windy/twrp-3.0.2-0-scorpion_windy.img.html) 
```
fastboot -S 256M flash fotakernel twrp-3.0.2-0-scorpion.img
```

* reboot your device
```
fastboot reboot
```

* enter recovery with press volume down while the LED is purple

* sideload [SailfishOS](http://195.201.99.111/sailfishos-scorpion_windy/sailfishos-scorpion_windy-release-2.2.1.18-Alpha1.zip)

```
adb sideload sailfishos-scorpion_windy-release-2.2.1.18-Alpha1.zip
```

* reboot and enjoy 